import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Window {
    private JPanel panel1;
    private JPanel center;
    private JPanel south;
    private JButton button1;
    private JTextField imie;
    private JTextField nazwisko;
    private JTextField wiek;
    private JSpinner numerButa;
    private JComboBox stan;
    private JRadioButton mezczyzna;
    private JRadioButton kobieta;
    private JLabel etykieta;
    private Formularz formularz;

    public JPanel getPanel1() {
        return panel1;
    }

    public Window() {


        mezczyzna.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stan.removeAllItems();
                stan.addItem("zonaty");
                stan.addItem("singiel");
            }
        });
        kobieta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stan.removeAllItems();
                stan.addItem("mezata");
                stan.addItem("singielka");
            }
        });

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Plec plec;
                if (mezczyzna.isSelected()) {
                    plec = Plec.MEZCZYZNA;
                } else plec = Plec.KOBIETA;
                formularz = new Formularz
                        (wiek.getText(), imie.getText(), nazwisko.getText(),
                                Integer.valueOf(numerButa.getValue().toString()), plec, stan.getSelectedItem().toString());
                System.out.println(formularz.toString());
                formularz.zapisDoPliku();
            }
        });
    }
}
