import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Formularz {
    private String wiek;
    private String imie;
    private String nazwisko;
    private int numerButa;
    private Plec plec;
    private String stanCywilny;

    public Formularz(String wiek, String imie, String nazwisko, int numerButa, Plec plec, String stanCywilny) {
        this.wiek = wiek;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.numerButa = numerButa;
        this.plec = plec;
        this.stanCywilny = stanCywilny;
    }
    public void zapisDoPliku(){
        try(PrintWriter pw = new PrintWriter(new File("output.txt"))){
            pw.println("Imie: "+imie);
            pw.println("Nazwisko: "+nazwisko);
            pw.println("Wiek: "+ wiek);
            pw.println("Numer Buta: "+numerButa);
            pw.println("Stan cywilny: "+stanCywilny);
            pw.println("Plec: "+ plec);

        }
        catch (FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
    }
    public void sformatujPrzedZapisem(){

    }

    @Override
    public String toString() {
        return "Formularz{" +
                "wiek='" + wiek + '\'' +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", numerButa=" + numerButa +
                ", plec=" + plec +
                ", stanCywilny='" + stanCywilny + '\'' +
                '}';
    }
}
